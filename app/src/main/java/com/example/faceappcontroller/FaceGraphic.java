package com.example.faceappcontroller;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;

public class FaceGraphic extends GraphicOverlay.Graphic {
    private Paint mFacePositionPaint;
    private volatile Face mFace;
    private float x = 0f;
    private float y = 0f;

    public FaceGraphic (GraphicOverlay overlay) {
        super(overlay);

        final int selectedColor = Color.GREEN;

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);
    }

    public void updateFace (Face face) {
        mFace = face;
        postInvalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if (face == null) {
            return;
        }
        
        for (Landmark landmark : face.getLandmarks()) {

            if (landmark.getType() == Landmark.NOSE_BASE){
                x = translateX(landmark.getPosition().x);
                y = translateY(landmark.getPosition().y);

                canvas.drawCircle(x, y, 7, mFacePositionPaint);
            }
        }
    }

    public float getX () {
        return x;
    }

    public float getY () {
        return y;
    }
}