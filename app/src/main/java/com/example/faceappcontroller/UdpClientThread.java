package com.example.faceappcontroller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UdpClientThread extends Thread {

    private String dstAddress;
    private String dstPort;
    private String dstMov;

    private DatagramSocket socket;
    private InetAddress address;

    public UdpClientThread(String addr, String port, String mov) {
        super();
        dstAddress = addr;
        dstPort = port;
        dstMov = mov;
    }

    @Override
    public void run(){

        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        try {
            socket.setReuseAddress(true);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        try {
            address = InetAddress.getByName(dstAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        byte[] buf = dstMov.getBytes();

        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, Integer.parseInt(dstPort));

        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
