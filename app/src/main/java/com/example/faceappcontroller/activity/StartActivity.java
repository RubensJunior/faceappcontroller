package com.example.faceappcontroller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.faceappcontroller.R;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StartActivity extends AppCompatActivity {

    private EditText ip;
    private EditText port;
    private SharedPreferences sharedPreferences;
    private int width = 0, height = 0;
    public static final String Ip = "ipKey";
    public static final String Port = "portKey";
    private static final Pattern ipAddress =
            Pattern.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
            + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
            + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
            + "|[1-9][0-9]|[0-9]))");

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ip  = findViewById(R.id.ip);
        port = findViewById(R.id.porta);

        sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE);

        if(sharedPreferences.contains(Ip))
            ip.setText(sharedPreferences.getString(Ip,""));

        if (sharedPreferences.contains(Port))
            port.setText(sharedPreferences.getString(Port,""));

        ipFilter();
        portFilter();

        final Bundle dados = getIntent().getExtras();
        if (dados != null){
            width = dados.getInt("width");
            height = dados.getInt("height");
        }

        Button iniciar = findViewById(R.id.iniciar);
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                boolean validation = validation();
                if (validation){
                    save();
                    Intent i = new Intent(getApplicationContext(), JoystickActivity.class);
                    i.putExtra("ip",ip.getText().toString());
                    i.putExtra("port",port.getText().toString());
                    i.putExtra("width", width);
                    i.putExtra("height", height);

                    startActivity(i);
                }
            }
        });

        Button redimensionar = findViewById(R.id.redimensionar);
        redimensionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                startActivity( new Intent(getApplicationContext(), ResizeActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menuAjuda){
            Intent i = new Intent(StartActivity.this, HelpActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.moveTaskToBack(true);
    }

    public void save(){
        String i = ip.getText().toString();
        String p = port.getText().toString();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Ip, i);
        editor.putString(Port, p);
        editor.apply();
    }

    public void ipFilter(){
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       android.text.Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart)
                            + source.subSequence(start, end)
                            + destTxt.substring(dend);
                    if (!resultingTxt
                            .matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (String split : splits) {
                            if (Integer.parseInt(split) > 255){
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        };

        ip.setFilters(filters);
    }

    public void portFilter(){
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       android.text.Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart)
                            + source.subSequence(start, end)
                            + destTxt.substring(dend);
                    if (!resultingTxt
                            .matches("^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$"))
                        return "";
                }
                return null;
            }
        };

        port.setFilters(filters);
    }

    public boolean validation(){
        boolean retorno = true;
        Matcher matcher = ipAddress.matcher(ip.getText().toString());

        if(TextUtils.isEmpty(ip.getText().toString())){
            ip.setError(getText(R.string.erroValores));
            retorno = false;
        }

        if (TextUtils.isEmpty(port.getText().toString())){
            port.setError(getText(R.string.erroPorta));
            retorno = false;
        }

        if(!matcher.matches()){
            ip.setError(getText(R.string.erroValores));
            retorno = false;
        }

        if (!isConnected(getApplicationContext())){
            Toast toast = Toast.makeText(getBaseContext(), R.string.erroInternet, Toast.LENGTH_SHORT);
            toast.show();
            retorno = false;
        }

        return retorno;
    }

    public boolean isConnected(@NotNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if ( cm != null ) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null && ni.isConnected();
        }
        return false;
    }
}
