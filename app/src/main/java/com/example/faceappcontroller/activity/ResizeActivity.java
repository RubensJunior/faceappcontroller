package com.example.faceappcontroller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.faceappcontroller.R;

public class ResizeActivity extends AppCompatActivity {

    private ScaleGestureDetector gestureDetector;
    public static int WIDTH = 0, HEIGHT = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resize);

        ConstraintLayout up = findViewById(R.id.up);
        ConstraintLayout down = findViewById(R.id.down);
        ConstraintLayout left = findViewById(R.id.left);
        ConstraintLayout right = findViewById(R.id.right);
        ConstraintLayout areaSelecionavel = findViewById(R.id.areaSelecionavel);

        WIDTH = up.getLayoutParams().width;
        HEIGHT = up.getLayoutParams().height;

        gestureDetector = new ScaleGestureDetector
                (this, new ScaleGestureListener(up, down, left, right));

        areaSelecionavel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch (View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        Button salvar = findViewById(R.id.salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent i = new Intent(getApplicationContext(), StartActivity.class);
                i.putExtra("width", WIDTH);
                i.putExtra("height", HEIGHT);
                startActivity(i);
            }
        });
    }

    private static class ScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener{

        ConstraintLayout up, down, left, right;
        float scale, onScaleBegin = 0, onScaleEnd = 0;
        int width, height;
        int newWidth, newHeight;

        public ScaleGestureListener(ConstraintLayout u, ConstraintLayout d, ConstraintLayout l, ConstraintLayout r) {
            super();
            up = u;
            down = d;
            left = l;
            right = r;
            scale = 1.0f;
            width = up.getLayoutParams().width;
            height = up.getLayoutParams().height;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();

            newWidth = width;
            newHeight = height;

            float MIN = 0.7f;
            float MAX = 1.3f;
            if (scale >= MIN && scale <= MAX){
                newWidth *= scale;
                newHeight *= scale;
                WIDTH = newWidth;
                HEIGHT = newHeight;
                updateLayouts();
            }

            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            onScaleBegin = scale;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            onScaleEnd = scale;
            super.onScaleEnd(detector);
        }

        public void updateLayouts(){
            up.getLayoutParams().width = WIDTH;
            up.getLayoutParams().height = HEIGHT;
            down.getLayoutParams().width = WIDTH;
            down.getLayoutParams().height = HEIGHT;
            left.getLayoutParams().width = WIDTH;
            left.getLayoutParams().height = HEIGHT;
            right.getLayoutParams().width = WIDTH;
            right.getLayoutParams().height = HEIGHT;

            up.requestLayout();
            down.requestLayout();
            left.requestLayout();
            right.requestLayout();
        }
    }
}