package com.example.faceappcontroller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.faceappcontroller.CameraSourcePreview;
import com.example.faceappcontroller.FaceGraphic;
import com.example.faceappcontroller.GraphicOverlay;
import com.example.faceappcontroller.JoystickView;
import com.example.faceappcontroller.R;
import com.example.faceappcontroller.UdpClientThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import me.drakeet.materialdialog.MaterialDialog;

public final class JoystickActivity extends AppCompatActivity {
    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    private MaterialDialog mMaterialDialog;
    private SharedPreferences sharedPreferences;
    public static JoystickView up;
    public static JoystickView down;
    public static JoystickView left;
    public static JoystickView right;
    public static final String widthSP = "widthKey";
    public static final String heightSP = "heightKey";

    private static final int RC_HANDLE_GMS = 9001;
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    public static int WIDTH = 0, HEIGHT = 0;

    private String ip;
    private String port;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_joystick);
        hideSystemUI();

        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.faceOverlay);
        sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE);
        up = findViewById(R.id.up);
        down = findViewById(R.id.down);
        left = findViewById(R.id.left);
        right = findViewById(R.id.right);

        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

        Bundle dados = getIntent().getExtras();
        assert dados != null;
        ip = dados.getString("ip");
        port = dados.getString("port");
        WIDTH = dados.getInt("width");
        HEIGHT = dados.getInt("height");

        if (WIDTH > 0 && HEIGHT > 0){
            updateLayouts();
            save();
        }else {
            if (sharedPreferences.contains(widthSP) && sharedPreferences.contains(heightSP)){
                WIDTH = sharedPreferences.getInt(widthSP, 0);
                HEIGHT = sharedPreferences.getInt(heightSP, 0);
                updateLayouts();
                save();
            }
        }
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public void save(){
        int w = WIDTH;
        int h = HEIGHT;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(widthSP, w);
        editor.putInt(heightSP, h);
        editor.apply();
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)){
            callDialog(new String[]{Manifest.permission.CAMERA} );
        }else{
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    RC_HANDLE_CAMERA_PERM);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        finish();
    }

    private void callDialog (final String[] permissions) {
        mMaterialDialog = new MaterialDialog(this)
                .setTitle("Permission")
                .setMessage(R.string.permissao)
                .setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick (View v) {
                        ActivityCompat.requestPermissions(JoystickActivity.this, permissions, RC_HANDLE_CAMERA_PERM);
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancelar, new View.OnClickListener() {
                    @Override
                    public void onClick (View v) {
                        mMaterialDialog.dismiss();
                        finish();
                    }
                });
        mMaterialDialog.show();
    }

    private void createCameraSource() {

        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setMode(FaceDetector.FAST_MODE)
                .setProminentFaceOnly(true)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(1024, 720)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(10.0f)
                .setAutoFocusEnabled(true)
                .build();
    }

    public boolean isConnected(@NotNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if ( cm != null ) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null && ni.isConnected();
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isConnected(getApplicationContext()))
            finish();

        startCameraSource();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }


    private void startCameraSource() {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    public void updateLayouts(){
        up.getLayoutParams().width = WIDTH;
        up.getLayoutParams().height = HEIGHT;
        down.getLayoutParams().width = WIDTH;
        down.getLayoutParams().height = HEIGHT;
        left.getLayoutParams().width = WIDTH;
        left.getLayoutParams().height = HEIGHT;
        right.getLayoutParams().width = WIDTH;
        right.getLayoutParams().height = HEIGHT;

        up.requestLayout();
        down.requestLayout();
        left.requestLayout();
        right.requestLayout();
    }

    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay,ip,port);
        }
    }

    private class GraphicFaceTracker extends Tracker<Face> {

        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;
        private UdpClientThread mUdpClient;
        private TextView direction;
        private String ip, port;

        @SuppressLint({"ResourceType", "SetTextI18n"})
        GraphicFaceTracker(GraphicOverlay overlay, String ip, String port) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
            direction = findViewById(R.id.Direction);

            this.ip = ip;
            this.port = port;
        }

        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);

            runOnUiThread(new Runnable() {

                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {

                    if (up.pointerContainsJoystick(mFaceGraphic.getX(),mFaceGraphic.getY())){
                        direction.setText(R.string.up);
                        up.setBackground(R.drawable.cima);
                        mUdpClient = new UdpClientThread(ip, port, "UP");
                        mUdpClient.start();
                    }else up.setBackground(R.drawable.button);

                    if (down.pointerContainsJoystick(mFaceGraphic.getX(),mFaceGraphic.getY())){
                        direction.setText(R.string.down);
                        down.setBackground(R.drawable.baixo);
                        mUdpClient = new UdpClientThread(ip, port, "DOWN");
                        mUdpClient.start();
                    }else down.setBackground(R.drawable.button);

                    if (left.pointerContainsJoystick(mFaceGraphic.getX(),mFaceGraphic.getY())){
                        direction.setText(R.string.left);
                        left.setBackground(R.drawable.esquerda);
                        mUdpClient = new UdpClientThread(ip, port, "LEFT");
                        mUdpClient.start();
                    }else left.setBackground(R.drawable.button);

                    if (right.pointerContainsJoystick(mFaceGraphic.getX(),mFaceGraphic.getY())){
                        direction.setText(R.string.right);
                        right.setBackground(R.drawable.direita);
                        mUdpClient = new UdpClientThread(ip, port, "RIGHT");
                        mUdpClient.start();
                    }else right.setBackground(R.drawable.button);
                }
            });
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }
}
