package com.example.faceappcontroller;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import androidx.constraintlayout.widget.ConstraintLayout;

public class JoystickView extends ConstraintLayout {

    ConstraintLayout mLayout;
    DisplayMetrics displayMetrics;

    public JoystickView (Context context) {
        super(context);
        mLayout = this;
        displayMetrics = new DisplayMetrics();
    }

    public JoystickView (Context context, AttributeSet attrs) {
        super(context, attrs);
        mLayout = this;
        displayMetrics = new DisplayMetrics();
    }

    public JoystickView (Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mLayout = this;
        displayMetrics = new DisplayMetrics();
    }

    public boolean pointerContainsJoystick(float x, float y){
        Rect viewRect = new Rect();
        int[] location = new int[2];
        this.getLocationInWindow(location);

        int left = location[0];
        int right = left + this.getMeasuredWidth();
        int top = location[1];
        int bottom = top + this.getMeasuredHeight();

        viewRect.set(left, top, right, bottom);
        return viewRect.contains((int) x, (int) y);
    }

    public void setBackground (int drawable){
        this.setBackgroundResource(drawable);
    }
}
